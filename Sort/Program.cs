﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Sort
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] data =
            {
                571, 50, 712, 134, 68, 235, 4, 820, 304, 764, 780, 929, 946, 448, 395, 609, 513, 635, 944, 348, 749,
                397, 283, 311, 692, 2, 704, 715, 841, 923, 258, 265, 2, 714, 376, 193, 240, 441, 657, 585, 197, 563,
                303, 963, 199, 114, 478, 848, 761, 678, 863, 255, 401, 471, 656, 966, 547, 596, 331, 198, 634, 188, 154,
                166, 618, 552, 2, 721, 196, 205, 992, 240, 61, 645, 444, 521, 551, 873, 481, 365, 647, 52, 352, 66, 117,
                867, 95, 256, 530, 977, 70, 94, 740, 56, 882, 701, 879, 46, 543, 787
            };
            //data = RadixSortLSD1(data); //1ая реализация
            data = RadixSortLSD2(data); //2ая реализация
            foreach (var item in data)
            {
                Console.WriteLine(item);
            }
        }

        static int[] RadixSortLSD2(int[] data)
        {
            var boxs = new List<int>[10];
            boxs = boxs.Select(x => x = new List<int>()).ToArray();
            var maxCountDischarge = data.Max().ToString().Length;

            for (var i = 0; i < maxCountDischarge; i++)
            {
                for (var j = 0; j < data.Length; j++)
                {
                    var number = data[j] % (int) Math.Pow(10, i + 1) / (int) Math.Pow(10, i);
                    boxs[number].Add(data[j]);
                }

                data = Convert(boxs);
                boxs = new List<int>[boxs.Length];
                boxs = boxs.Select(x => x = new List<int>()).ToArray();
            }

            return data;
        }

        public static int[] Convert(List<int>[] from)
        {
            var result = new List<int>();
            for (var i = 0; i < from.Length; i++)
            {
                for (var j = 0; j < from[i].Count; j++)
                {
                    result.Add(from[i][j]);
                }
            }

            return result.ToArray();
        }

        static int[] RadixSortLSD1(int[] data) //для положительных чисел
        {
            int maxCountDischarge = SearchMaxCountDischarge(data); //узнаем max кол-во разрядов в числе
            for (int i = 1; i <= maxCountDischarge; i++)
            {
                List<Queue<int>> box = new List<Queue<int>>();
                for (int j = 0; j < data.Length; j++)
                {
                    PutNumberIntoBoxs(box, data, i, maxCountDischarge, j);
                }

                data = TakeNumberInBoxs(box, data);
            }

            return data;
        }

        private static int[] TakeNumberInBoxs(List<Queue<int>> box, int[] data)
        {
            int[] newData = new int[data.Length];
            int i = 0;
            foreach (var queue in box)
            {
                foreach (var number in queue)
                {
                    newData[i] = number;
                    i++;
                }
            }

            return newData;
        }

        private static void PutNumberIntoBoxs(List<Queue<int>> box, int[] data, int currentDischarge,
            int maxCountDischarge, int j)
        {
            int number = data[j];
            int numberBox = 0;
            int countCurrentNumber = SearchCountDischerge(number); //находит кол-во разрядов в числе
            if (maxCountDischarge >= countCurrentNumber)
            {
                if (currentDischarge <= countCurrentNumber)
                {
                    numberBox = number / (int) (Math.Pow(10, currentDischarge - 1)) % 10;
                }
            }

            while (numberBox > box.Count - 1)
            {
                box.Add(new Queue<int>());
            }

            box[numberBox].Enqueue(data[j]);
        }

        private static int SearchCountDischerge(int number) //находит кол-во разрядов в числе
        {
            int countNumber = 0;
            while (number > 0)
            {
                number /= 10;
                countNumber++;
            }

            return countNumber;
        }

        private static int SearchMaxCountDischarge(int[] data) //находит max кол-во разрядов во всем массиве
        {
            int count = 0;
            for (int i = 0; i < data.Length; i++)
            {
                int number = data[i];
                int countCurrentNumber = 0;
                while (number > 0)
                {
                    number /= 10;
                    countCurrentNumber++;
                }

                if (countCurrentNumber > count)
                {
                    count = countCurrentNumber;
                }
            }

            return count;
        }
    }
}